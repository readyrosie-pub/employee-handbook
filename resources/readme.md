# Resources

## Building Schema and Understanding the ReadyRosie Approach to Families and Educators

ReadyRosie believes in Ready Families, Ready Educators, Ready Children by building powerful partnerships between families and educators.  

Our approach is to build relationships. We believe that to be successful we must build relationships; relationships with families, relationships with educators, and facilitate relationships between educators and families and in so doing we will help close the achievement gap.

ReadyRosie’s work is based on, but not limited to, research by the following people:

### 1. Family Engagement
Dr. Karen L. Mapp, Dual Capacity Framework; 
* [Linking Family Engagement to Learning (YouTube)](https://www.youtube.com/watch?v=KDPY1t8E6Cg)
* [Reframing Our View About Our Families (YouTube)](https://youtu.be/KqHMNT2UtW0)
* [“Unlocking Families’ Potential: A Conversation with Karen L. Mapp” Educational Leadership, September 2017, Vol. 75 NO.1](https://assets.readyrosie.com/Unlocking-Families-Potential-A-Conversation-with-Karen-L-Mapp.pdf)
* [Joyce Epstein’s School-Family-Community Partnership Model](https://www.enotes.com/research-starters/joyce-epsteins-school-family-community-partnership#research-starter-research-starter); 6 types of involvement 

Reflection for Family Engagement section:  
 1. How do the ReadyRosie Modeled Moments and Family Workshops support the different roles of the family and encourage family engagement?

### 2. Strengthening Families 
Dr. Tara Yosso, Center for Collaborative Research for an Equitable California
* [Summary of Yosso’s Cultural Wealth Model](https://assets.readyrosie.com/Summary-of-Yossos-Cultural-Wealth.pdf)
* [About Strengthening Families and the Protective Factors Framework, Center for the Study of Social Policy](https://assets.readyrosie.com/About-Strengthening-Families.pdf)

Reflection for Strengthening Families section: 
 1.	Which cultural wealth(s) do you believe you have? 
 2.	Does ReadyRosie address any of the activities listed in the “Everyday Activities” section? How?

### 3. Effects of Poverty and Stress 
* [The Statisticks Lottery (YouTube)](https://youtu.be/52KuhCpiOUE)
* [Reducing Inequality Through Education (Podcast)](https://poverty.ucdavis.edu/podcast/reducing-inequality-through-education)
* [Paul Tough's Helping Children Succeed Overview (YouTube)](https://www.youtube.com/watch?v=W2kBHSC9kro)
* [Helping Children Succeed; What Works and Why, Paul Tough](https://assets.readyrosie.com/Helping-Children-Succeed-Paul-Tough.pdf) (the whole book is great but particularly the first 12-13 chapters)

Reflection for Effects of Poverty and Stress section:
 1.	When you think about poverty and stress, how does it impact your role at ReadyRosie?
 2.	What do you need to keep in mind, if anything?

### 4. Race and Biases
* [“Test Yourself for Hidden Biases”](https://www.tolerance.org/professional-development/test-yourself-for-hidden-bias) Teaching Tolerance (take the Race test)
* [“I Don’t Think I’m Biased”](https://www.tolerance.org/magazine/spring-2010/i-dont-think-im-biased) Teaching Tolerance

Reflection for Race and Biases section:
 1.	Did anything surprise you after taking the Race test? 
 2.	Did you find you had a ‘hidden’ bias?

### Overall Reflection:
 1.	What was an overall ‘aha’ you had?
 2.	What is your biggest take-away from the resources?
 3.	How does this information about working with families and especially families of poverty affect your work at ReadyRosie?
 4.	Is there any area that you want to learn more about or do better based on your learning?
 5.	Based on your review of the resources, how do you think ReadyRosie does in trying to close the achievement gap?

## Attached Required Readings
* [About Strengthening Families and the Protective Factors Framework](https://assets.readyrosie.com/About-Strengthening-Families.pdf)
* [Helping Children Succeed](https://assets.readyrosie.com/Helping-Children-Succeed-Paul-Tough.pdf)
* [Summary of Yosso's Cultural Wealth Model](https://assets.readyrosie.com/Summary-of-Yossos-Cultural-Wealth.pdf)
* [Whose culture has capital? A critical race theory discussion of community culture wealth](https://assets.readyrosie.com/Yosso-Whose-Culture-has-Capital.pdf)
* [Unlocking Families Potential](https://assets.readyrosie.com/Unlocking-Families-Potential-A-Conversation-with-Karen-L-Mapp.pdf)

### Suggested Additional Resources: 
 1.	Mapp, Karen, Lander, Jessica, Carver, Ilene. (2017). Powerful Partnerships: A Teachers Guide to Engaging Families for Student Success.
 2.	[Working Toward Well-Being Community Approached to Toxic Stress](https://www.youtube.com/watch?v=Amwp5uO_MfU)
 3.	[Show About Race: What Have We Learned Podcast](https://www.showaboutrace.com/episodes/1704)
 4.	[“Beyond the Classroom; The Impact of Culture on the Classroom”](http://www.miamiherald.com/news/local/community/miami-dade/community-voices/article36727782.html)




