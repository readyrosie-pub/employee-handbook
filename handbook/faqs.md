# FAQs
## How do I get started working at ReadyRosie?
### Welcome
First of all, we're so happy you’re here! You will be working with a tightknit group of professionals who are passionate about ReadyRosie’s mission and long term vision. We hustle hard and laugh hard too. We’re excited about our jobs, with all the challenges and rewards they bring, each and every day, and we hope you will be excited about your job too.
### Contract/Hellosign
The first step of your onboarding process will be signing off on your contract. We use Hellosign as our document signing tool. When your contract is drafted, you’ll be invited to sign off on it by Hellosign. Please sign off asap for our records.
### Email
Prior to your first day, you will be assigned an email address. If it’s available, your email will be firstname@readyrosie.com. We manage email through our G-Suite account, so your inbox will be accessible via mail.google.com. ReadyRosie emails are distributed to contractors on an as-needed basis.
### Slack
New employees and contractors will receive an invite to Slack. We use Slack to stay connected, share ideas, files, screenshots, pictures from a conference, all sorts of things! We encourage you to sign in and introduce yourself, asap.
### Gusto
You will also receive an invite to Gusto to your personal email address, our contractor/employee payroll system. The link in your invite will push you to fill out your profile. Gusto provides the following:

*For employess…*
- Access to paystubs and direct deposit.
- Sign up for benefits, if eligible.
- View W-9
- Options for giving

*For contractors…*
- Access to paystubs and direct deposit.
- View 1099 at the end of the year -- Please note, taxes will not automatically be taken out of your wages via Gusto. As a contractor, you’re responsible to pay on your own.

Please keep your info in Gusto updated, specifically edit your profile whenever you move primary addresses, change your name, and/or number of dependents.
### Harvest
Harvest is the system we use for contractors and hourly employees to submit time and expenses. If you need to track your time (i.e. not working on a flat day rate or project rate), you should receive an invite to our Harvest account in your inbox. 
You can log into our Harvest account via https://readyrosiecontractors.harvestapp.com/
## When can I sign up for benefits? And How?
If you’re a 30+ hourly or salary employee, you are eligible for ReadyRosie benefits. Upon filling out your employee information, Gusto will invite you to opt into one of our group medical insurance plans. When you opt in for benefits, your coverage will be “active” starting the first of the month after hire date.
Health insurance is provided in the US via Blue Cross Blue Shield - Texas (HMO and PPO plans). ReadyRosie covers up to 75% of the premium of Blue Choice Silver PPO (2017). Employee pays the difference in premium if they select a more expensive plan.
Open enrollment is in late November/early December every year. Marriages and domestic partnerships are covered equally.
## When do I get paid?
Employees are paid twice per month (semi-monthly) on the 15th and last business day of each month.

Contractors should submit their Harvest time or invoice between the 15th and 18th of each month for speediest payment turnaround. Please send your invoice to jessica@readyrosie.com.
## When can I take vacation?
ReadyRosie offers two weeks of paid vacation, a few extra personal days to use at your discretion, and the standard national holidays every year. This is a guideline to make sure you take time off, so if you need a couple extra days, no problem. We don’t track your days off; we use the honor system. Just make sure to check with your team before taking any extended absence, and do plan and coordinate in advance.
## What about laptops and software?
ReadyRosie will provide new employees with company-owned hardware, including but not limited to laptops, tablets, chargers, cases, presentation remotes, speakers, cameras, etc. Certain hardware is provided based on employee's role within the company. 

Computers and accompanying accessories for existing employees will be rolled out in phases based on priority.

Company-owned software licenses will also be provided as needed (ex. Microsoft Office Suite, Adobe Acrobat). Additional licenses or subscriptions should be requested through your manager.
