# Employee Handbook

Put handbook intro content here.

- [Our Mission](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/our-mission.md)
- [What Influenced Us](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/what-influenced-us.md)
- [ReadyRosie's Story](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/our-story.md)
- [Benefits](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/benefits.md)
- [Getting Started](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/getting-started.md)
- [How We Work and Growth Feedback](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/how-we-work.md)
- [Policies](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/policies.md)
- [FAQs](https://gitlab.com/readyrosie-pub/employee-handbook/blob/master/handbook/faqs.md)